# libCube #

View Project:
[Live Demo](http://cubepokemon.aerobatic.io/) | [View Source](https://bitbucket.org/ElBrad/pokemon/src)


### Announcement ###

I have decided to make this project open source. Feel free to use the code in this repository for your own projects but please leave me a credit where appropriate.

Love,

Brad

### README ###

A fan-made pokemon game created on-top of libCube the 2.5D javascript game-engine written by Bradley Duncan (Cube)

The goal of this project is to create a multi-platform version of the popular game Pokemon, using the original 1st generation games Red, Blue, Yellow as a guide then expanding to Gold / Silver and maybe eventually emerald.

The game will be multiplayer online on the same world with many different players on the same map. **different lobbies?**

### What do I need to get started? ###

* A webserver (i.e nginx, apache, xampp) --or a browser with XHR cross-origin disabled
* A modern web-browser (Chrome, Safari, Opera work best)
* Tiled Map Editor (www.mapeditor.org)
* A love of pokemon

### Contribution Guidelines ###

* Engine Development
* Gameplay Development
* Artwork
* Gameplay ideas
* Writing tests
* Code review
* Wiki Documentation

### Officially Supported Deployment Platforms ###

* Mac
* PC
* iOS
* Android
* PS4 (Needs testing)

### Active Developers ###

* Bradley Duncan (Cube) - Lead Programmer